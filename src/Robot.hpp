#ifndef ROBOT_HPP_
#define ROBOT_HPP_

#include "Config.hpp"

#include "AbstractAgent.hpp"
#include "AStar.hpp"
#include "BoundedVector.hpp"
#include "Message.hpp"
#include "MessageHandler.hpp"
#include "Observer.hpp"
#include "Point.hpp"
#include "Size.hpp"
#include "Region.hpp"
#include "Matrix.hpp"

#include <iostream>
#include <memory>
#include <mutex>
#include <string>
#include <thread>
#include <random>

namespace Messaging
{
	class Message;
	class Server;
	typedef std::shared_ptr< Server > ServerPtr;
}

namespace Model
{
	class Robot;
	typedef std::shared_ptr<Robot> RobotPtr;

	class Goal;
	typedef std::shared_ptr<Goal> GoalPtr;

	class Robot :	public AbstractAgent,
					public Messaging::MessageHandler,
					public Base::Observer
	{
		public:
			/**
			 *
			 */
			Robot();
			/**
			 *
			 */
			explicit Robot( const std::string& aName);
			/**
			 *
			 */
			Robot(	const std::string& aName,
					const Point& aPosition);
			/**
			 *
			 */
			virtual ~Robot();
			/**
			 *
			 */
			std::string getName() const
			{
				return name;
			}
			/**
			 *
			 */
			void setName( const std::string& aName,
						  bool aNotifyObservers = true);
			/**
			 *
			 */
			Size getSize() const;
			/**
			 *
			 */
			void setSize(	const Size& aSize,
							bool aNotifyObservers = true);
			/**
			 *
			 */
			Point getPosition() const
			{
				return position;
			}
			/**
			 *
			 */
			void setPosition(	const Point& aPosition,
								bool aNotifyObservers = true);
			/**
			 *
			 */
			BoundedVector getFront() const;
			/**
			 *
			 */
			void setFront(	const BoundedVector& aVector,
							bool aNotifyObservers = true);
			/**
			 *
			 */
			float getSpeed() const;
			/**
			 *
			 */
			void setSpeed( float aNewSpeed,
						   bool aNotifyObservers = true);
			/**
			 *
			 * @return true if the robot is acting, i.e. either planning or driving
			 */
			bool isActing() const
			{
				return acting;
			}
			/**
			 *
			 */
			virtual void startActing() override;
			/**
			 *
			 */
			virtual void stopActing() override;
			/**
			 *
			 * @return true if the robot is driving
			 */
			bool isDriving() const
			{
				return driving;
			}
			/**
			 *
			 */
			virtual void startDriving();
			/**
			 *
			 */
			virtual void stopDriving();
			/**
			 *
			 * @return true if the robot is communicating, i.e. listens with an active ServerConnection
			 */
			bool isCommunicating() const
			{
				return communicating;
			}
			/**
			 * Starts a ServerConnection that listens at port 12345 unless given
			 * an other port by specifying a command line argument -local_port=port
			 */
			void startCommunicating();
			/**
			 * Connects to the ServerConnection that listens at port 12345 unless given
			 * an other port by specifying a command line argument -local_port=port
			 * and sends a message with messageType "1" and a body with "stop"
			 *
			 */
			void stopCommunicating();
			/**
			 *
			 */
			Region getRegion() const;
			/**
			 *
			 */
			bool intersects( const Region& aRegion) const;
			/**
			 *
			 */
			Point getFrontLeft() const;
			/**
			 *
			 */
			Point getFrontRight() const;
			/**
			 *
			 */
			Point getBackLeft() const;
			/**
			 *
			 */
			Point getBackRight() const;
			/**
			 * @name Observer functions
			 */
			//@{
			/**
			 * A Notifier will call this function if this Observer will handle the notifications of that
			 * Notifier. It is the responsibility of the Observer to filter any events it is interested in.
			 *
			 */
			virtual void handleNotification() override;
			//@}
			/**
			 *
			 */
			PathAlgorithm::OpenSet getOpenSet() const
			{
				return astar.getOpenSet();
			}
			/**
			 *
			 */
			PathAlgorithm::Path getPath() const
			{
				return path;
			}
			/**
			 * @name Messaging::MessageHandler functions
			 */
			//@{
			/**
			 * This function is called by a ServerSesssion whenever a message is received. If the request is handled,
			 * any response *must* be set in the Message argument. The message argument is then echoed back to the
			 * requester, probably a ClientSession.
			 *
			 * @see Messaging::RequestHandler::handleRequest( Messaging::Message& aMessage)
			 */
			virtual void handleRequest( Messaging::Message& aMessage);
			/**
			 * This function is called by a ClientSession whenever a response to a previous request is received.
			 *
			 * @see Messaging::ResponseHandler::handleResponse( const Messaging::Message& aMessage)
			 */
			virtual void handleResponse( const Messaging::Message& aMessage);
			//@}
			/**
			 * @name Debug functions
			 */
			//@{
			/**
			 * Returns a 1-line description of the object
			 */
			virtual std::string asString() const override;
			/**
			 * Returns a description of the object with all data of the object usable for debugging
			 */
			virtual std::string asDebugString() const override;
			//@}

			//for chance and random numbers (such as normal distributions)
			std::random_device rand{};
			std::mt19937 rdGen{rand()};

			//values for RNG
			float stdevCompass = 2;
			float stdevOdometer = 1;
			float stdevLidar = 10;

			int measurementStepSize = 5;

			int worldMinX = 0;
			int worldMaxX = 1024;
			int worldMinY = 0;
			int worldMaxY = 1024;

			bool visualiseLidar = false;

			Point prevPosition;

			Point positionEstimate;
			Point positionPrevious;
			float positionDiffX;
			float positionDiffY;

			//paths to keep track of
			std::vector<Point> robotPathActual;
			std::vector<Point> robotPathKalman;
			std::vector<Point> robotPathParticle;
			std::vector<Point> lidarPoints;

			enum OrientationMethod {
				DEFAULT,
				KALMAN,
				PARTICLE
			} currentOrientationMethod = KALMAN;

			enum Direction {
				NORTH,
				NORTHEAST,
				EAST,
				SOUTHEAST,
				SOUTH,
				SOUTHWEST,
				WEST,
				NORTHWEST
			};

			/**
			 * function for printing debugging information.
			 * Given string(@param msg) will be printed in the robotworld application logger
			 * and the regular console.
			 */
			void debugPrint(std::string msg);
			/**
			 * Toggles the lidar visualisation boolean on and off.
			 * This is used in the draw function in the canvas to visualise the sensor
			 * for debugging mostly.
			 */
			void toggleLidarVisual();

			/**
			 * Generates a normal distribution out of given @param avg (average) and
			 * @param stdev (standard deviation)
			 */
			float generateNormalDist(float avg, float stdev);
			/**
			 * Generates a uniform distribution out of given @param min
			 * and @param max values.
			 */
			float generateUniformDist(float min, float max);
			/**
			 * sets the robot previous position.
			 * Previous and current position can be used for getting robot angles, speed and
			 * movement predictions.
			 */
			void savePrevPosition();
			/**
			 * Set the active filter to be used (kalman or particle)
			 */
			void selectFilter(OrientationMethod filter);

			/**
			 * Generates a compass value (angle) with sensor noise.
			 */
			float generateCompassVal();
			/**
			 * Generates an odometer value with sensor noise.
			 */
			float generateOdometerVal();
			/**
			 * Calculates the angle between given @param firstpoint and @param secondpoint
			 */
			float getAngle(Point firstPoint, Point secondPoint); //calculates the actual angle without sensor noise.
			/**
			 * simulates a lidar scan (with sensor noise). Calculates the distance from given @param point center
			 * for every angle per @param degreesperscan (default 2 degrees) and stores them in
			 * a vector @return std::vector<float>. This function makes use of the functions
			 * 'calculateIntersectionFromAngle and localizeObstaclePoint'
			 */
			std::vector<float> generateLidarVals(Point center, int degreesPerScan);
			/**
			 * calculates the distance where and intersection occurs from given @param angle
			 * and given @param point of origin. It is possible to save the points at which such an
			 * intersection occurred by setting @param collectLidarPoints. This is only recommended
			 * when using this function for generating a complete lidar scan with the appropriate function.
			 * This function uses 'localizeObstaclePoint' to calculate the exact points of intersection.
			 */
			float calculateIntersectionFromAngle(float angle, Point origin, bool collectLidarPoints);
			/**
			 * localizes the exact point at which an intersection occurred. To do this a given
			 * @param gradient is needed and a distance where the intersection is found (@param intersect)
			 * at a given @param Direction and from a given @param origin point.
			 */
			Point localizeObstaclePoint(float gradient, float intersect, enum Direction, Point origin);
			/**
			 * calculates the largest value out of all the collected distance values.
			 */
			float calculateLargestAngleDistance();

			std::vector<Point> getParticlePath()
			{
				return robotPathParticle;
			}

			std::vector<Point> getKalmanPath()
			{
				return robotPathKalman;
			}

			std::vector<Matrix<float, 2, 1>> getParticles()
			{
				return particles;
			}

			std::vector<Point> getLidarData()
			{
				return lidarPoints;
			}

			// kalman and particle filter functions and variables
			Matrix<float, 2, 2> identityMatrix = {{1, 0}, {0, 1}};
			Matrix<float, 2, 1> stateVectorKalman;
			Matrix<float, 2, 2> processError;

			/**
			 * applies the kalman filter to the given @param kalmanStateVector and @param kalmanProcessError.
			 * the given @param kalmanUpdate and @param kalmanMeasurement are used to update the incoming values and
			 * the first two given parameters. The first two paramters are given by reference and will be changed by
			 * this function as an output.
			 */
			void kalmanFilter(Matrix<float, 2, 1>& kalmanStateVector, Matrix<float, 2, 2>& kalmanProcessError, Matrix<float, 2, 1> kalmanUpdate, Matrix<float, 2, 1> kalmanMeasurement);

			std::size_t amountOfParticles = 5000;
			std::vector<Matrix<float, 2, 1>> particles;
			Matrix<float, 2, 1> particleUpdateMatrix;
			Matrix<float, 2, 1> stateVectorParticle;
			Matrix<float, 2, 2> processErrorParticle;
			long long weightFactor = 100;

			/**
			 * removes and old particles and generates a new set of 'amoutOfparticles' in
			 * order to run the particlefilter.
			 */
			void initializeParticleFilter();
			/**
			 * resamples every particle and sets new particle weights.
			 * updates the particles vector according to the given @param particleUpdate.
			 */
			void particleFilter(Matrix<float, 2, 1> particleUpdate);
			Matrix<float, 2, 1> maxWeightParticle;
			Matrix<float, 2, 1> getHighestWeightingParticle();
			/**
			 * calculates the cosine similarity of given vectors of floats @param a and @param b.
			 * This is used by the particlefilter to update the particle weights.
			 */
			float cosineSimilarity(std::vector<float> a, std::vector<float> b);

			float firstChance = 0.7;
			float firstRange = 1;
			float secondChance = 0.3;
			float secondRange = 2;
			float controlUpdateStandardDeviation = firstChance * firstRange + secondChance * secondRange + 0.2;

			unsigned short timePerMove = 50;
			float moveXBelief;
			float moveYBelief;
			float moveXReal;
			float moveYReal;
			float moveXMeasured;
			float moveYMeasured;
			std::size_t measurementStepCycle = 5;

			/**
			 * Simulates a movement by the robot in given @param angle direction.
			 * This updates (reads)the robot sensors and generates a new belief of its next position.
			 */
			void move(float angle);
			Matrix<float, 2, 1> stateVector;
			/**
			 * clears any paths, particles and visual aspects belonging to the robot.
			 */
			void cleanup();

		protected:
			/**
			 *
			 */
			void drive();
			/**
			 *
			 */
			void calculateRoute(GoalPtr aGoal);
			/**
			 *
			 */
			bool arrived(GoalPtr aGoal);
			/**
			 *
			 */
			bool collision();
		private:
			/**
			 *
			 */
			std::string name;
			/**
			 *
			 */
			Size size;
			/**
			 *
			 */
			Point position;
			/**
			 *
			 */
			BoundedVector front;
			/**
			 *
			 */
			float speed;
			/**
			 *
			 */
			GoalPtr goal;
			/**
			 *
			 */
			PathAlgorithm::AStar astar;
			/**
			 *
			 */
			PathAlgorithm::Path path;
			/**
			 *
			 */
			bool acting;
			/**
			 *
			 */
			bool driving;
			/**
			 *
			 */
			bool communicating;
			/**
			 *
			 */
			std::thread robotThread;
			/**
			 *
			 */
			mutable std::recursive_mutex robotMutex;
			/**
			 *
			 */
			Messaging::ServerPtr server;
	};
} // namespace Model
#endif // ROBOT_HPP_
