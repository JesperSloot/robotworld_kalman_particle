#include <cassert>
#include <stdexcept>
#include <numeric>
#include <cmath>
#include <utility>
#include <iomanip>

/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, N >::Matrix( T value)
{
	for (std::size_t row = 0; row < M; ++row)
	{
		for (std::size_t column = 0; column < N; ++column)
		{
			matrix.at( row).at( column) = value;
		}
	}
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, N >::Matrix( const std::initializer_list< T >& aList)
{
	// Check the arguments
	assert( aList.size() == M * N);

	auto row_iter = aList.begin();
	for (std::size_t row = 0; row < M; ++row)
	{
		for (std::size_t column = 0; column < N; ++column, ++row_iter)
		{
			matrix.at( row).at( column) = *row_iter;
		}
	}
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, N >::Matrix( const std::initializer_list< std::initializer_list< T > >& aList)
{
	// Check the arguments, the static assert assures that there is at least 1 M and 1 N!
	assert( aList.size() == M && (*aList.begin()).size() == N);

	auto row_iter = aList.begin();
	for (std::size_t row = 0; row < aList.size(); ++row, ++row_iter)
	{
		auto column_iter = (*row_iter).begin();
		for (std::size_t column = 0; column < (*row_iter).size(); ++column, ++column_iter)
		{
			matrix.at( row).at( column) = *column_iter;
		}
	}
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, N >::Matrix( const Matrix< T, M, N >& aMatrix) :
				matrix( aMatrix.matrix)
{
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
std::array< T, N >& Matrix< T, M, N >::at( std::size_t aRowIndex)
{
	return matrix.at( aRowIndex);
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
const std::array< T, N >& Matrix< T, M, N >::at( std::size_t aRowIndex) const
{
	return matrix.at( aRowIndex);
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
T& Matrix< T, M, N >::at( 	std::size_t aRowIndex,
							std::size_t aColumnIndex)
{
	return matrix.at( aRowIndex).at( aColumnIndex);
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
const T& Matrix< T, M, N >::at( std::size_t aRowIndex,
								std::size_t aColumnIndex) const
{
	return matrix.at( aRowIndex).at( aColumnIndex);
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
std::array< T, N >& Matrix< T, M, N >::operator[]( std::size_t aRowIndex)
{
	return matrix[aRowIndex];
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
const std::array< T, N >& Matrix< T, M, N >::operator[]( std::size_t aRowIndex) const
{
	return matrix[aRowIndex];
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, N >& Matrix< T, M, N >::operator=( const Matrix< T, M, N >& rhs)
{
	if (this != &rhs)
	{
		matrix = rhs.matrix;
	}
	return *this;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
bool Matrix< T, M, N >::operator==( const Matrix< T, M, N >& rhs) const
{
	return matrix == rhs.matrix;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
template< class T2 >
Matrix< T, M, N >& Matrix< T, M, N >::operator*=( const T2& scalar)
{
	static_assert( std::is_arithmetic<T2>::value, "Value T2 must be arithmetic, see http://en.cppreference.com/w/cpp/types/is_arithmetic");

	for (std::size_t row = 0; row < M; ++row)
	{
		for (std::size_t column = 0; column < N; ++column)
		{
			matrix.at( row).at( column) *= scalar;
		}
	}
	return *this;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
template< class T2 >
Matrix< T, M, N > Matrix< T, M, N >::operator*( const T2& scalar) const
{
	static_assert( std::is_arithmetic<T2>::value, "Value T2 must be arithmetic, see http://en.cppreference.com/w/cpp/types/is_arithmetic");

	Matrix< T, M, N > result( *this);
	return result *= scalar;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
template< class T2 >
Matrix< T, M, N >& Matrix< T, M, N >::operator/=( const T2& aScalar)
{
	static_assert( std::is_arithmetic<T2>::value, "Value T2 must be arithmetic, see http://en.cppreference.com/w/cpp/types/is_arithmetic");

	for (std::size_t row = 0; row < M; ++row)
	{
		for (std::size_t column = 0; column < N; ++column)
		{
			matrix.at( row).at( column) /= aScalar;
		}
	}
	return *this;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
template< class T2 >
Matrix< T, M, N > Matrix< T, M, N >::operator/( const T2& aScalar) const
{
	static_assert( std::is_arithmetic<T2>::value, "Value T2 must be arithmetic, see http://en.cppreference.com/w/cpp/types/is_arithmetic");

	Matrix< T, M, N > result( *this);
	return result /= aScalar;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, N >& Matrix< T, M, N >::operator+=( const Matrix< T, M, N >& rhs)
{
	for (std::size_t row = 0; row < M; ++row)
	{
		for (std::size_t column = 0; column < N; ++column)
		{
			matrix[row][column] += rhs.at( row, column);
		}
	}
	return *this;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, N > Matrix< T, M, N >::operator+( const Matrix< T, M, N >& rhs) const
{
	Matrix< T, M, N > result( *this);
	return result += rhs;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, N >& Matrix< T, M, N >::operator-=( const Matrix< T, M, N >& rhs)
{
	for (std::size_t row = 0; row < M; ++row)
	{
		for (std::size_t column = 0; column < N; ++column)
		{
			matrix[row][column] -= rhs.at( row, column);
		}
	}
	return *this;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, N > Matrix< T, M, N >::operator-( const Matrix< T, M, N >& rhs) const
{
	Matrix< T, M, N > result( *this);
	return result -= rhs;
}
/**
 * (M, N) * (N, P) -> (M, P)
 */
template< typename T, std::size_t M, std::size_t N >
template< std::size_t columns >
Matrix< T, M, columns > Matrix< T, M, N >::operator*( const Matrix< T, N, columns >& rhs) const
{
	Matrix< T, M, columns > result;
	if (getColumns() != rhs.getRows()) {
		return result;
	}

	for (std::size_t row = 0; row < getRows(); ++row)
	{
		for (std::size_t column = 0; column < rhs.getColumns(); ++column)
		{
			auto value = 0;
			// For-loop through all numbers that need to be multiplied with rhs
			for (std::size_t index = 0; index < rhs.getRows(); ++index)
			{
				value += (at(row, index) * rhs.at(index, column));
			}

			result.at(row, column) = value;
		}
	}

	return result;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, N, M > Matrix< T, M, N >::transpose() const
{
	Matrix< T, N, M > result;
	for (std::size_t row = 0; row < N; ++row)
	{
		for (std::size_t column = 0; column < M; ++column)
		{
			result.at(row, column) = at(column, row);
		}
	}

	return result;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, N > Matrix< T, M, N >::identity() const
{
	Matrix< T, N, M > result;
	if (getRows() != getColumns())
	{
		return result;
	}

	for (std::size_t row = 0; row < getRows(); ++row)
	{
		for (std::size_t column = 0; column < getColumns(); ++column)
		{
			if (row == column)
			{
				result.at(row, column) = 1;
			}
			else
			{
				result.at(row, column) = 0;
			}
		}
	}

	return result;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, N > Matrix< T, M, N >::gauss() const
{
	Matrix< T, M, N > result;

	// Copy this.matrix
	for (std::size_t row = 0; row < result.getRows(); ++row)
	{
		for (std::size_t column = 0; column < result.getColumns(); ++column)
		{
			result.at(row, column) = matrix[row][column];
		}
	}

	for (std::size_t pivot = 0; pivot < getColumns() - 1; ++pivot)
	{
		// Step 1
		std::size_t highestPivotIndex = pivot;
		auto highestPivot = result.at(pivot, pivot);
		for (std::size_t row = pivot + 1; row < getRows(); ++row)
		{
			if (result.at(row, pivot) > highestPivot)
			{
				highestPivot = result.at(row, pivot);
				highestPivotIndex = row;
			}
		}
		result.at(pivot).swap(result.at(highestPivotIndex));

		// Step 2
		auto pivotDivideNumber = result.at(pivot, pivot);
		for (std::size_t column = 0; column < getColumns(); ++column)
		{
			result.at(pivot, column) = result.at(pivot, column) / pivotDivideNumber;
		}

		// Step 3
		for (std::size_t row = pivot + 1; row < getRows(); ++row)
		{
			auto multiplicationNumber = result.at(row, pivot);
			for (std::size_t column = 0; column < getColumns(); ++column)
			{
				result.at(row, column) = result.at(row, column) - (result.at(pivot, column) * multiplicationNumber);
			}
		}
	}

	return result;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, N > Matrix< T, M, N >::gaussJordan() const
{
	Matrix< T, M, N > result;

	// Copy this.matrix
	for (std::size_t row = 0; row < result.getRows(); ++row)
	{
		for (std::size_t column = 0; column < result.getColumns(); ++column)
		{
			result.at(row, column) = matrix[row][column];
		}
	}

	for (std::size_t pivot = 0; pivot < getRows(); ++pivot)
	{
		// Step 1
		std::size_t highestPivotIndex = pivot;
		auto highestPivot = result.at(pivot, pivot);
		for (std::size_t row = pivot + 1; row < getRows(); ++row)
		{
			if (result.at(row, pivot) > highestPivot)
			{
				highestPivot = result.at(row, pivot);
				highestPivotIndex = row;
			}
		}
		result.at(pivot).swap(result.at(highestPivotIndex));

		// Step 2
		auto pivotDivideNumber = result.at(pivot, pivot);
		for (std::size_t column = 0; column < getColumns(); ++column)
		{
			result.at(pivot, column) = result.at(pivot, column) / pivotDivideNumber;
		}

		// Step 3
		for (std::size_t row = 0; row < getRows(); ++row)
		{
			if (row != pivot)
			{
				auto multiplicationNumber = result.at(row, pivot);
				for (std::size_t column = 0; column < getColumns(); ++column)
				{
					result.at(row, column) = result.at(row, column) - (result.at(pivot, column) * multiplicationNumber);
				}
			}
			
		}
	}

	return result;
}
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, 1 > Matrix< T, M, N >::solve() const
{
	Matrix < T, M, 1 > result;
	Matrix < T, M, N > tempMatrix = *this;

	tempMatrix = tempMatrix.gaussJordan();

	for (std::size_t row = 0; row < getRows(); ++row)
	{
		result.at(row, 0) = tempMatrix.at(row, getColumns() - 1);
	}

	return result;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, N > Matrix< T, M, N >::inverse() const
{
	Matrix< T, N, M > result;
	Matrix< T, M, N * 2 > expansion;

	for (std::size_t row = 0; row < expansion.getRows(); ++row)
	{
		for (std::size_t column = 0; column < getColumns(); ++column)
		{
			expansion.at(row, column) = matrix[row][column];
		}

		for (std::size_t column = getColumns(); column < expansion.getColumns(); ++column)
		{
			if (row == column - getColumns())
			{
				expansion.at(row, column) = 1;
			}
			else
			{
				expansion.at(row, column) = 0;
			}
		}
	}

	expansion = expansion.gaussJordan();

	for (std::size_t row = 0; row < result.getRows(); ++row)
	{
		for (std::size_t column = 0; column < result.getColumns(); ++column)
		{
			result.at(row, column) = expansion.at(row, column + getColumns());
		}
	}

	return result;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
std::string Matrix< T, M, N >::to_string() const
{
	std::string result = "Matrix<" + std::to_string( N) + "," + std::to_string( M) + ">\n{\n";
	for (std::size_t i = 0; i < M; ++i)
	{
		for (std::size_t j = 0; j < N; ++j)
		{
			result += std::to_string( matrix[i][j]) + ",";
		}
		result += "\n";
	}
	result += "}";
	return result;
}
/**
 *
 */
template< typename T, const std::size_t N >
bool equals(	const Matrix< T, 1, N >& lhs,
				const Matrix< T, 1, N >& rhs,
				const T aPrecision /*= std::numeric_limits<T>::epsilon()*/,
				const unsigned long aFactor /*= 1*/)
{
	// TODO Implement this function
	return true;
}
/**
 *
 */
template< typename T, const std::size_t M >
bool equals(	const Matrix< T, M, 1 >& lhs,
				const Matrix< T, M, 1 >& rhs,
				const T aPrecision /*= std::numeric_limits<T>::epsilon()*/,
				const unsigned long aFactor /*= 1*/)
{
	// TODO Implement this function
	return true;
}
/**
 *
 */
template< typename T, const std::size_t M, const std::size_t N >
bool equals(	const Matrix< T, M, N >& lhs,
				const Matrix< T, M, N >& rhs,
				const T aPrecision /*= std::numeric_limits<T>::epsilon()*/,
				const unsigned long aFactor /*= 1*/)
{
	// TODO Implement this function
	return true;
}
