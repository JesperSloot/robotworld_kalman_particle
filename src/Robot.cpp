#include "Robot.hpp"

#include "Client.hpp"
#include "CommunicationService.hpp"
#include "Goal.hpp"
#include "LaserDistanceSensor.hpp"
#include "Logger.hpp"
#include "MainApplication.hpp"
#include "MathUtils.hpp"
#include "Message.hpp"
#include "MessageTypes.hpp"
#include "RobotWorld.hpp"
#include "Server.hpp"
#include "Shape2DUtils.hpp"
#include "Wall.hpp"
#include "WayPoint.hpp"

#include <chrono>
#include <ctime>
#include <sstream>
#include <thread>

namespace Model
{
	/**
	 *
	 */
	Robot::Robot() :
								name( ""),
								size( DefaultSize),
								position( DefaultPosition),
								front( 0, 0),
								speed( 0.0),
								acting(false),
								driving(false),
								communicating(false),
								prevPosition(DefaultPosition)
	{
		std::shared_ptr< AbstractSensor > laserSensor( new LaserDistanceSensor( this));
		attachSensor( laserSensor);
	}
	/**
	 *
	 */
	Robot::Robot( const std::string& aName) :
								name( aName),
								size( DefaultSize),
								position( DefaultPosition),
								front( 0, 0),
								speed( 0.0),
								acting(false),
								driving(false),
								communicating(false),
								prevPosition(DefaultPosition)
	{
		std::shared_ptr< AbstractSensor > laserSensor( new LaserDistanceSensor( this));
		attachSensor( laserSensor);
	}
	/**
	 *
	 */
	Robot::Robot(	const std::string& aName,
					const Point& aPosition) :
								name( aName),
								size( DefaultSize),
								position( aPosition),
								front( 0, 0),
								speed( 0.0),
								acting(false),
								driving(false),
								communicating(false),
								prevPosition(DefaultPosition)
	{
		std::shared_ptr< AbstractSensor > laserSensor( new LaserDistanceSensor( this));
		attachSensor( laserSensor);
	}
	/**
	 *
	 */
	Robot::~Robot()
	{
		if(driving)
		{
			stopDriving();
		}
		if(acting)
		{
			stopActing();
		}
		if(communicating)
		{
			stopCommunicating();
		}
		cleanup();
	}
	/**
	 *
	 */
	void Robot::setName( const std::string& aName,
						 bool aNotifyObservers /*= true*/)
	{
		name = aName;
		if (aNotifyObservers == true)
		{
			notifyObservers();
		}

	}
	/**
	 *
	 */
	Size Robot::getSize() const
	{
		return size;
	}
	/**
	 *
	 */
	void Robot::setSize(	const Size& aSize,
							bool aNotifyObservers /*= true*/)
	{
		size = aSize;
		if (aNotifyObservers == true)
		{
			notifyObservers();
		}
	}
	/**
	 *
	 */
	void Robot::setPosition(	const Point& aPosition,
								bool aNotifyObservers /*= true*/)
	{
		position = aPosition;
		if (aNotifyObservers == true)
		{
			notifyObservers();
		}
	}
	/**
	 *
	 */
	BoundedVector Robot::getFront() const
	{
		return front;
	}
	/**
	 *
	 */
	void Robot::setFront(	const BoundedVector& aVector,
							bool aNotifyObservers /*= true*/)
	{
		front = aVector;
		if (aNotifyObservers == true)
		{
			notifyObservers();
		}
	}
	/**
	 *
	 */
	float Robot::getSpeed() const
	{
		return speed;
	}
	/**
	 *
	 */
	void Robot::setSpeed( float aNewSpeed,
						  bool aNotifyObservers /*= true*/)
	{
		speed = aNewSpeed;
		if (aNotifyObservers == true)
		{
			notifyObservers();
		}
	}
	/**
	 *
	 */
	void Robot::startActing()
	{
		acting = true;
		std::thread newRobotThread( [this]{	startDriving();});
		robotThread.swap( newRobotThread);
	}
	/**
	 *
	 */
	void Robot::stopActing()
	{
		acting = false;
		driving = false;
		robotThread.join();
	}
	/**
	 *
	 */
	void Robot::startDriving()
	{
		driving = true;

		goal = RobotWorld::getRobotWorld().getGoal( "Goal");
		calculateRoute(goal);

		drive();

		if(acting)
		{
			acting = false;
		}
	}
	/**
	 *
	 */
	void Robot::stopDriving()
	{
		driving = false;
	}
	/**
	 *
	 */
	void Robot::startCommunicating()
	{
		if(!communicating)
		{
			communicating = true;

			std::string localPort = "12345";
			if (Application::MainApplication::isArgGiven( "-local_port"))
			{
				localPort = Application::MainApplication::getArg( "-local_port").value;
			}

			if(Messaging::CommunicationService::getCommunicationService().isStopped())
			{
				TRACE_DEVELOP( "Restarting the Communication service");
				Messaging::CommunicationService::getCommunicationService().restart();
			}

			server = std::make_shared<Messaging::Server>(	static_cast<unsigned short>(std::stoi(localPort)),
															toPtr<Robot>());
			Messaging::CommunicationService::getCommunicationService().registerServer( server);
		}
	}
	/**
	 *
	 */
	void Robot::stopCommunicating()
	{
		if(communicating)
		{
			communicating = false;

			std::string localPort = "12345";
			if (Application::MainApplication::isArgGiven( "-local_port"))
			{
				localPort = Application::MainApplication::getArg( "-local_port").value;
			}

			Messaging::Client c1ient( 	"localhost",
										static_cast<unsigned short>(std::stoi(localPort)),
										toPtr<Robot>());
			Messaging::Message message( Messaging::StopCommunicatingRequest, "stop");
			c1ient.dispatchMessage( message);
		}
	}
	/**
	 *
	 */
	Region Robot::getRegion() const
	{
		Point translatedPoints[] = { getFrontRight(), getFrontLeft(), getBackLeft(), getBackRight() };
		return Region( 4, translatedPoints);
	}
	/**
	 *
	 */
	bool Robot::intersects( const Region& aRegion) const
	{
		Region region = getRegion();
		region.Intersect( aRegion);
		return !region.IsEmpty();
	}
	/**
	 *
	 */
	Point Robot::getFrontLeft() const
	{
		// x and y are pointing to top left now
		int x = position.x - (size.x / 2);
		int y = position.y - (size.y / 2);

		Point originalFrontLeft( x, y);
		double angle = Utils::Shape2DUtils::getAngle( front) + 0.5 * Utils::PI;

		Point frontLeft( static_cast<int>((originalFrontLeft.x - position.x) * std::cos( angle) - (originalFrontLeft.y - position.y) * std::sin( angle) + position.x),
						 static_cast<int>((originalFrontLeft.y - position.y) * std::cos( angle) + (originalFrontLeft.x - position.x) * std::sin( angle) + position.y));

		return frontLeft;
	}
	/**
	 *
	 */
	Point Robot::getFrontRight() const
	{
		// x and y are pointing to top left now
		int x = position.x - (size.x / 2);
		int y = position.y - (size.y / 2);

		Point originalFrontRight( x + size.x, y);
		double angle = Utils::Shape2DUtils::getAngle( front) + 0.5 * Utils::PI;

		Point frontRight( static_cast<int>((originalFrontRight.x - position.x) * std::cos( angle) - (originalFrontRight.y - position.y) * std::sin( angle) + position.x),
						  static_cast<int>((originalFrontRight.y - position.y) * std::cos( angle) + (originalFrontRight.x - position.x) * std::sin( angle) + position.y));

		return frontRight;
	}
	/**
	 *
	 */
	Point Robot::getBackLeft() const
	{
		// x and y are pointing to top left now
		int x = position.x - (size.x / 2);
		int y = position.y - (size.y / 2);

		Point originalBackLeft( x, y + size.y);

		double angle = Utils::Shape2DUtils::getAngle( front) + 0.5 * Utils::PI;

		Point backLeft( static_cast<int>((originalBackLeft.x - position.x) * std::cos( angle) - (originalBackLeft.y - position.y) * std::sin( angle) + position.x),
						static_cast<int>((originalBackLeft.y - position.y) * std::cos( angle) + (originalBackLeft.x - position.x) * std::sin( angle) + position.y));

		return backLeft;

	}
	/**
	 *
	 */
	Point Robot::getBackRight() const
	{
		// x and y are pointing to top left now
		int x = position.x - (size.x / 2);
		int y = position.y - (size.y / 2);

		Point originalBackRight( x + size.x, y + size.y);

		double angle = Utils::Shape2DUtils::getAngle( front) + 0.5 * Utils::PI;

		Point backRight( static_cast<int>((originalBackRight.x - position.x) * std::cos( angle) - (originalBackRight.y - position.y) * std::sin( angle) + position.x),
						 static_cast<int>((originalBackRight.y - position.y) * std::cos( angle) + (originalBackRight.x - position.x) * std::sin( angle) + position.y));

		return backRight;
	}
	/**
	 *
	 */
	void Robot::handleNotification()
	{
		//	std::unique_lock<std::recursive_mutex> lock(robotMutex);

		static int update = 0;
		if ((++update % 200) == 0)
		{
			notifyObservers();
		}
	}
	/**
	 *
	 */
	void Robot::handleRequest( Messaging::Message& aMessage)
	{
		FUNCTRACE_TEXT_DEVELOP(aMessage.asString());

		switch(aMessage.getMessageType())
		{
			case Messaging::StopCommunicatingRequest:
			{
				aMessage.setMessageType(Messaging::StopCommunicatingResponse);
				aMessage.setBody("StopCommunicatingResponse");
				// Handle the request. In the limited context of this works. I am not sure
				// whether this works OK in a real application because the handling is time sensitive,
				// i.e. 2 async timers are involved:
				// see CommunicationService::stopServer and Server::stopHandlingRequests
				Messaging::CommunicationService::getCommunicationService().stopServer(12345,true);

				break;
			}
			case Messaging::EchoRequest:
			{
				aMessage.setMessageType(Messaging::EchoResponse);
				aMessage.setBody( "Messaging::EchoResponse: " + aMessage.asString());
				break;
			}
			default:
			{
				TRACE_DEVELOP(__PRETTY_FUNCTION__ + std::string(": default not implemented"));
				break;
			}
		}
	}
	/**
	 *
	 */
	void Robot::handleResponse( const Messaging::Message& aMessage)
	{
		FUNCTRACE_TEXT_DEVELOP(aMessage.asString());

		switch(aMessage.getMessageType())
		{
			case Messaging::StopCommunicatingResponse:
			{
				//Messaging::CommunicationService::getCommunicationService().stop();
				break;
			}
			case Messaging::EchoResponse:
			{
				break;
			}
			default:
			{
				TRACE_DEVELOP(__PRETTY_FUNCTION__ + std::string( ": default not implemented, ") + aMessage.asString());
				break;
			}
		}
	}
	/**
	 *
	 */
	std::string Robot::asString() const
	{
		std::ostringstream os;

		os << "Robot " << name << " at (" << position.x << "," << position.y << ")";

		return os.str();
	}
	/**
	 *
	 */
	std::string Robot::asDebugString() const
	{
		std::ostringstream os;

		os << "Robot:\n";
		os << AbstractAgent::asDebugString();
		os << "Robot " << name << " at (" << position.x << "," << position.y << ")\n";

		return os.str();
	}
	/**
	 *
	 */
	void Robot::drive()
	{
		try
		{
			for (std::shared_ptr< AbstractSensor > sensor : sensors)
			{
				//sensor->setOn();
			}

			if (speed == 0.0)
			{
				speed = 10.0;
			}

			// clear existing paths
			robotPathParticle.clear();
			robotPathKalman.clear();
			// initialize filters and input values.
			initializeParticleFilter();
			particleFilter({0, 0});
			processError = {{1, 0}, {0, 1}};
			// update initial particles
			particleFilter(stateVector);

			stateVector = {position.x, position.y};
			stateVectorKalman = stateVector;
			stateVectorParticle = stateVector;
			processErrorParticle = processError;

			Point stateVectorInitialBelief = Point(stateVector.at(0, 0), stateVector.at(1, 0));
			robotPathParticle.push_back(stateVectorInitialBelief);
			robotPathKalman.push_back(stateVectorInitialBelief);

			unsigned pathPoint = 0;

			while (position.x > worldMinX && position.x < worldMaxX && position.y > worldMinY && position.y < worldMaxY && pathPoint < path.size())
			{
				//TODO: check if a set number of movement steps is needed here...
				for(std::size_t i = 0; i < measurementStepCycle; ++i)
				{
					generateLidarVals(position, 2);
					const PathAlgorithm::Vertex& vertex = path[pathPoint+=static_cast<int>(speed)];
					// update sensor outputs and beliefs
					move(getAngle(Point(stateVector.at(0, 0), stateVector.at(1, 0)), vertex.asPoint()));

					front = BoundedVector( vertex.asPoint(), position);
					position.x = vertex.x;
					position.y = vertex.y;

					if (arrived(goal) || collision())
					{
						Application::Logger::log(__PRETTY_FUNCTION__ + std::string(": arrived or collision"));
						notifyObservers();
						position = goal->getPosition();
						stopActing();
						break;
					}

					Matrix<float, 2, 1> updateMatrix = {moveXBelief, moveYBelief};
					Matrix<float, 2, 1> measurementMatrix = {moveXMeasured, moveYMeasured};

					kalmanFilter(particleUpdateMatrix, processErrorParticle, updateMatrix, measurementMatrix);
					robotPathParticle.push_back(Point(stateVectorParticle.at(0, 0) + particleUpdateMatrix.at(0, 0), stateVectorParticle.at(1, 0) + particleUpdateMatrix.at(1, 0)));

					measurementMatrix += stateVectorKalman;
					kalmanFilter(stateVectorKalman, processError, updateMatrix, measurementMatrix);

					robotPathKalman.push_back(Point(stateVectorKalman.at(0, 0), stateVectorKalman.at(1, 0)));

					std::this_thread::sleep_for(std::chrono::milliseconds(timePerMove));
				}
				notifyObservers();
				//std::this_thread::sleep_for( std::chrono::milliseconds( 100));
				particleFilter(particleUpdateMatrix);
				stateVectorParticle = getHighestWeightingParticle();
				particleUpdateMatrix = {0, 0};

				if(currentOrientationMethod == PARTICLE)
				{
					stateVector = stateVectorParticle;
				}
				else if(currentOrientationMethod == KALMAN)
				{
					stateVector = stateVectorKalman;
				}
				else
				{
					debugPrint("WARN: no orientation method set.");
					stateVector = stateVectorParticle;
				}

				// this should be the last thing in the loop
				if(driving == false)
				{
					return;
				}
			} // while

			for (std::shared_ptr< AbstractSensor > sensor : sensors)
			{
				//sensor->setOff();
			}
		}
		catch (std::exception& e)
		{
			Application::Logger::log( __PRETTY_FUNCTION__ + std::string(": ") + e.what());
			std::cerr << __PRETTY_FUNCTION__ << ": " << e.what() << std::endl;
		}
		catch (...)
		{
			Application::Logger::log( __PRETTY_FUNCTION__ + std::string(": unknown exception"));
			std::cerr << __PRETTY_FUNCTION__ << ": unknown exception" << std::endl;
		}
	}
	/**
	 *
	 */
	void Robot::calculateRoute(GoalPtr aGoal)
	{
		path.clear();
		if (aGoal)
		{
			// Turn off logging if not debugging AStar
			Application::Logger::setDisable();

			front = BoundedVector( aGoal->getPosition(), position);
			//handleNotificationsFor( astar);
			path = astar.search( position, aGoal->getPosition(), size);
			//stopHandlingNotificationsFor( astar);

			Application::Logger::setDisable( false);
		}
	}
	/**
	 *
	 */
	bool Robot::arrived(GoalPtr aGoal)
	{
		if (aGoal && intersects( aGoal->getRegion()))
		{
			return true;
		}
		return false;
	}
	/**
	 *
	 */
	bool Robot::collision()
	{
		Point frontLeft = getFrontLeft();
		Point frontRight = getFrontRight();
		Point backLeft = getBackLeft();
		Point backRight = getBackRight();

		const std::vector< WallPtr >& walls = RobotWorld::getRobotWorld().getWalls();
		for (WallPtr wall : walls)
		{
			if (Utils::Shape2DUtils::intersect( frontLeft, frontRight, wall->getPoint1(), wall->getPoint2()) ||
							Utils::Shape2DUtils::intersect( frontLeft, backLeft, wall->getPoint1(), wall->getPoint2())	||
							Utils::Shape2DUtils::intersect( frontRight, backRight, wall->getPoint1(), wall->getPoint2()))
			{
				return true;
			}
		}
		const std::vector< RobotPtr >& robots = RobotWorld::getRobotWorld().getRobots();
		for (RobotPtr robot : robots)
		{
			if ( getObjectId() == robot->getObjectId())
			{
				continue;
			}
			if(intersects(robot->getRegion()))
			{
				return true;
			}
		}
		return false;
	}

	void Robot::debugPrint(std::string msg)
	{
		Application::Logger::log(__PRETTY_FUNCTION__ + msg);
		std::cout << msg << std::endl;
	}

	float Robot::generateNormalDist(float avg, float stdev)
	{
		// calculate normal distribution
		std::normal_distribution<> dist{avg, stdev};
		// randomize and return
		return float(std::round(dist(rdGen)));
	}

	float Robot::generateUniformDist(float min, float max)
	{
		// calculate uniform distribution
		std::uniform_real_distribution<> dist(min, max);
		// randomize and return
		return float(std::round(dist(rdGen)));
	}

	void Robot::savePrevPosition()
	{
		prevPosition = position;
	}

	float Robot::generateCompassVal()
	{
		// calculate initial orientation
		float tempDeg = float(atan((float)abs(getFrontRight().x - getFrontLeft().x) / (float)abs(getFrontLeft().y - getFrontRight().y)) * 180 / M_PI);
		// apply direction offset
		if (getFrontLeft().x < getFrontRight().x && getFrontLeft().y < getFrontRight().y)
		{
			tempDeg = 90.0f - tempDeg;
		}
		else if (getFrontLeft().x > getFrontRight().x && getFrontLeft().y < getFrontRight().y)
		{
			tempDeg = 90.0f + tempDeg;
		}
		else if (getFrontLeft().x > getFrontRight().x && getFrontLeft().y > getFrontRight().y)
		{
			tempDeg = 270.0f - tempDeg;
		}
		else if (getFrontLeft().x < getFrontRight().x && getFrontLeft().y > getFrontRight().y)
		{
			tempDeg = 270.0f + tempDeg;
		}
		//apply normal distribution(sensor noise) and return the result.
		return generateNormalDist(tempDeg, stdevCompass);
	}

	float Robot::generateOdometerVal()
	{
		positionDiffX = abs(position.x - positionPrevious.x);
		positionDiffY = abs(position.y - positionPrevious.y);

		return generateNormalDist(sqrt(pow(positionDiffX, 2) + pow(positionDiffY, 2)), stdevOdometer);
	}

	std::vector<float> Robot::generateLidarVals(Point center, int degreesPerScan)
	{
		std::vector<float> outputVals;
		lidarPoints.clear();
		for(auto i = 0; i < 360; i += degreesPerScan)
		{
			outputVals.push_back(generateNormalDist(calculateIntersectionFromAngle(i, center, true), stdevLidar));
		}

		return outputVals;
	}

	float Robot::getAngle(Point firstPoint, Point secondPoint)
	{
		float angle = 0.0;

		if (firstPoint.x - secondPoint.x == 0)
		{
			if (firstPoint.y > secondPoint.y)
			{
				// North
				angle = 0;
			}
			else if (firstPoint.y < secondPoint.y)
			{
				// South
				angle = 180;
			}
		}
		else if (firstPoint.y - secondPoint.y == 0)
		{
			if (firstPoint.x > secondPoint.x)
			{
				// West
				angle = 270;
			}
			else if (firstPoint.x < secondPoint.x)
			{
				// East
				angle = 90;
			}
		}

		if (firstPoint.x < secondPoint.x && firstPoint.y > secondPoint.y)
		{
			// North east
			angle = atan((float)abs(firstPoint.x - secondPoint.x) / (float)abs(firstPoint.y - secondPoint.y)) * 180 / M_PI;
		}
		else if (firstPoint.x < secondPoint.x && firstPoint.y < secondPoint.y)
		{
			// South east
			angle = atan((float)abs(firstPoint.y - secondPoint.y) / (float)abs(firstPoint.x - secondPoint.x)) * 180 / M_PI;
			angle += 90;
		}
		else if (firstPoint.x > secondPoint.x && firstPoint.y < secondPoint.y)
		{
			// South west
			angle = atan((float)abs(firstPoint.x - secondPoint.x) / (float)abs(firstPoint.y - secondPoint.y)) * 180 / M_PI;
			angle += 180;
		}
		else if (firstPoint.x > secondPoint.x && firstPoint.y > secondPoint.y)
		{
			// North west
			angle = atan((float)abs(firstPoint.y - secondPoint.y) / (float)abs(firstPoint.x - secondPoint.x)) * 180 / M_PI;
			angle += 270;
		}

		return angle;
	}

	float Robot::calculateIntersectionFromAngle(float angle, Point origin, bool collectLidarPoints)
	{
		float dist = 0.0;
		float gradient = 0.0;
		enum Direction dir = NORTH;

		if (angle > 0 && angle < 90)
		{
			gradient = float(tan(angle * M_PI / 180) * -1);
			gradient = float(pow(gradient, -1));
			dir = NORTHEAST;
		}
		else if (angle > 90 && angle < 180)
		{
			gradient = float(tan((angle - 90.0) * M_PI / 180));
			dir = SOUTHEAST;
		}
		else if (angle > 180 && angle < 270)
		{
			gradient = float(tan((angle - 180.0) * M_PI / 180) * -1);
			gradient = float(pow(gradient, -1));
			dir = SOUTHWEST;
		}
		else if (angle > 270 && angle < 360)
		{
			gradient = float(tan((angle - 270.0) * M_PI / 180));
			dir = NORTHWEST;
		}
		else if (angle == 0)
		{
			dir = NORTH;
		}
		else if (angle == 90)
		{
			dir = EAST;
		}
		else if (angle == 180)
		{
			dir = SOUTH;
		}
		else if (angle == 270)
		{
			dir = WEST;
		}

		float intersect = float(origin.y - (gradient * origin.x));
		Point obstaclePoint = localizeObstaclePoint(gradient, intersect, dir, origin);
		if(collectLidarPoints)
		{
			//fill the lidar points vector to be visualised. Function has to be called in the getlidarvalues function to properly fill all angles.
			lidarPoints.push_back(obstaclePoint);
		}

		dist = float(sqrt(pow(obstaclePoint.x - origin.x, 2) + pow(obstaclePoint.y - origin.y, 2)));
		return dist;
	}

	Point Robot::localizeObstaclePoint(float gradient, float intersect, enum Direction dir, Point origin)
	{
		Point obstaclePoint;
		float limitX = -1.0;
		float limitY = -1.0;

		switch (dir)
		{
		case NORTH:
			obstaclePoint.x = origin.x;
			obstaclePoint.y = worldMinY;
			break;
		case NORTHEAST:
			limitX = worldMaxX;
			limitY = worldMinY;
			break;
		case EAST:
			obstaclePoint.x = worldMaxX;
			obstaclePoint.y = origin.y;
			break;
		case SOUTHEAST:
			limitX = worldMaxX;
			limitY = worldMaxY;
			break;
		case SOUTH:
			obstaclePoint.x = origin.x;
			obstaclePoint.y = worldMaxY;
			break;
		case SOUTHWEST:
			limitX = worldMinX;
			limitY = worldMaxY;
			break;
		case WEST:
			obstaclePoint.x = worldMinX;
			obstaclePoint.y = origin.y;
			break;
		case NORTHWEST:
			limitX = worldMinX;
			limitY = worldMinY;
			break;
		}

		if (limitX != -1.0 && limitY != -1.0)
		{
			if ((limitX * gradient + intersect > limitY && limitY <= 0) || (limitX * gradient + intersect < limitY && limitY > 0))
			{
				obstaclePoint.x = limitX;
				obstaclePoint.y = limitX * gradient + intersect;
			}
			else
			{
				obstaclePoint.x = (limitY - intersect) / gradient;
				obstaclePoint.y = limitY;
			}
		}
		// check for an intersection with a wall or end of the world
		Point tmpPoint;
				const std::vector< WallPtr >& walls = RobotWorld::getRobotWorld().getWalls();
				for (WallPtr wall : walls)
				{
					if (Utils::Shape2DUtils::intersect(origin, obstaclePoint, wall->getPoint1(), wall->getPoint2()))
					{
						tmpPoint = Utils::Shape2DUtils::getIntersection(origin, obstaclePoint, wall->getPoint1(), wall->getPoint2());

						switch (dir)
						{
						case NORTH:
							if (tmpPoint.y > obstaclePoint.y)
							{
								obstaclePoint = tmpPoint;
							}
							break;
						case NORTHEAST:
							if (tmpPoint.x < obstaclePoint.x && tmpPoint.y > obstaclePoint.y)
							{
								obstaclePoint = tmpPoint;
							}
							break;
						case EAST:
							if (tmpPoint.x < obstaclePoint.x)
							{
								obstaclePoint = tmpPoint;
							}
							break;
						case SOUTHEAST:
							if (tmpPoint.x < obstaclePoint.x && tmpPoint.y < obstaclePoint.y)
							{
								obstaclePoint = tmpPoint;
							}
							break;
						case SOUTH:
							if (tmpPoint.y < obstaclePoint.y)
							{
								obstaclePoint = tmpPoint;
							}
							break;
						case SOUTHWEST:
							if (tmpPoint.x > obstaclePoint.x && tmpPoint.y < obstaclePoint.y)
							{
								obstaclePoint = tmpPoint;
							}
							break;
						case WEST:
							if (tmpPoint.x > obstaclePoint.x)
							{
								obstaclePoint = tmpPoint;
							}
							break;
						case NORTHWEST:
							if (tmpPoint.x > obstaclePoint.x && tmpPoint.y > obstaclePoint.y)
							{
								obstaclePoint = tmpPoint;
							}
							break;
						}
					}
				}

				return obstaclePoint;
	}

	float Robot::calculateLargestAngleDistance()
	{
		float angleDist = 0.0;
		float prevAngleDist;
		float maxScanDist = 0.0;
		float prevMaxScanDist = 0.0;
		std::vector<float> lidarScanData = generateLidarVals(position, 2);
		for(auto i = 0; i < lidarScanData.size(); ++i)
		{
			if(lidarScanData[i] > maxScanDist)
			{
				prevAngleDist = angleDist;
				angleDist = float(i)*2.0;
				prevMaxScanDist = maxScanDist;
				maxScanDist = lidarScanData[i];
			}
			else if(lidarScanData[i] < (measurementStepSize * speed + 20) && (i * 2 - 45) <= angleDist)
			{
				angleDist = prevAngleDist;
				maxScanDist = prevMaxScanDist;
			}
		}
		return angleDist;
	}

	void Robot::selectFilter(OrientationMethod filter)
	{
		if(filter == KALMAN)
		{
			Application::Logger::log("Now using Kalman filter");
			currentOrientationMethod = KALMAN;
			//TODO: activate the filter
		}
		else if(filter == PARTICLE)
		{
			Application::Logger::log("Now using Particle filter");
			currentOrientationMethod = PARTICLE;
			//TODO: activate the filter
		}
		else {
			Application::Logger::log("WARN: While selecting a filter: invalid input given.");
		}
	}

	void Robot::toggleLidarVisual()
	{
		visualiseLidar =! visualiseLidar;
	}

	void Robot::kalmanFilter(Matrix<float, 2, 1>& kalmanStateVector, Matrix<float, 2, 2>& kalmanProcessError, Matrix<float, 2, 1> kalmanUpdate, Matrix<float, 2, 1> kalmanMeasurement)
	{
		Matrix<float, 2, 1> predictedProcessNoiseVector = {controlUpdateStandardDeviation, controlUpdateStandardDeviation};
		Matrix<float, 2, 2> processNoiseCovariantionMatrix = predictedProcessNoiseVector * predictedProcessNoiseVector.transpose();
		processNoiseCovariantionMatrix.at(0, 1) = 0;
		processNoiseCovariantionMatrix.at(1, 0) = 0;

		// Control update
		kalmanStateVector = identityMatrix * kalmanStateVector + identityMatrix * kalmanUpdate + predictedProcessNoiseVector;
		kalmanProcessError = identityMatrix * kalmanProcessError * identityMatrix.transpose() + processNoiseCovariantionMatrix;

		Matrix<float, 2, 1> measurementNoise = {stdevCompass * stdevOdometer, stdevCompass * stdevOdometer};
		Matrix<float, 2, 2> kalmanGainError = measurementNoise * measurementNoise.transpose();
		kalmanGainError.at(0, 1) = 0;
		kalmanGainError.at(1, 0) = 0;

		// Kalman gain
		Matrix<float, 2, 2> kalmanGain = (kalmanProcessError * identityMatrix) * (identityMatrix * kalmanProcessError * identityMatrix.transpose() + kalmanGainError).inverse();

		// Measurement update
		kalmanMeasurement = identityMatrix * kalmanMeasurement + measurementNoise;

		kalmanStateVector = kalmanStateVector + kalmanGain * (kalmanMeasurement - identityMatrix * kalmanStateVector);
		kalmanProcessError = (identityMatrix - kalmanGain * identityMatrix) * kalmanProcessError;
	}

	void Robot::initializeParticleFilter()
	{
		particles.clear();
		for (std::size_t i = 0; i < amountOfParticles; ++i)
		{
			Matrix<float, 2, 1> particle = {generateUniformDist(worldMinX, worldMaxX), generateUniformDist(worldMinY, worldMaxY)};
			particles.push_back(particle);
		}
	}

	/**
	 * New function
	 */
	void Robot::particleFilter(Matrix<float, 2, 1> particleUpdate)
	{
		std::vector<float> lidarValuesRobot = generateLidarVals(position, 2);
		std::vector<long long> weightPerParticle;
		long long totalWeight = 0;
		long long maxWeight = 0;

		for (Matrix<float, 2, 1>& particle : particles)
		{
			// Control update
			particle += particleUpdate;
			Matrix<float, 2, 1> updateNoise = {generateNormalDist(0, controlUpdateStandardDeviation), generateNormalDist(0, controlUpdateStandardDeviation)};
			particle += updateNoise;

			if (particle.at(0, 0) > worldMinX && particle.at(0, 0) < worldMaxX && particle.at(1, 0) > worldMinY && particle.at(1, 0) < worldMaxY)
			{
				// Measurement update
				std::vector<float> lidarValuesParticle = generateLidarVals(Point(particle.at(0, 0), particle.at(1, 0)), 2);
				long long weight = pow(cosineSimilarity(lidarValuesRobot, lidarValuesParticle), 2) * weightFactor;
				totalWeight += weight;
				weightPerParticle.push_back(weight);
				if (weight > maxWeight)
				{
					maxWeight = weight;
					maxWeightParticle = {particle.at(0, 0), particle.at(1, 0)};
				}
			}
			else
			{
				weightPerParticle.push_back(0);
			}
		}

		// Resample particles
		std::vector<Matrix<float, 2, 1>> newParticles;

		for (std::size_t i = 0; i < particles.size(); ++i)
		{
			long long particleChance = generateUniformDist(1, totalWeight);
			long long weightCounter = 0;
			for (std::size_t j = 0; j < weightPerParticle.size(); ++j)
			{
				weightCounter += weightPerParticle[j];
				if (particleChance <= weightCounter)
				{
					newParticles.push_back(particles[j]);
					break;
				}
			}
		}

		particles = newParticles;
	}

	Matrix<float, 2, 1> Robot::getHighestWeightingParticle()
	{
		return maxWeightParticle;
	}

	float Robot::cosineSimilarity(std::vector<float> a, std::vector<float> b)
	{
		if (a.size() != b.size())
		{
			return -1; // Invalid inputvectors
		}

		double aTimesB = 0;
		double aTotal = 0;
		double bTotal = 0;

		for (std::size_t i = 0; i < a.size(); ++i)
		{
			aTimesB += a[i] * b[i];
			aTotal += pow(a[i], 2);
			bTotal += pow(b[i], 2);
		}

		return (aTimesB / (sqrt(aTotal) * sqrt(bTotal)));
	}

	void Robot::move(float angle)
	{
		moveXBelief = std::round(speed * sin(angle * M_PI / 180));
		moveYBelief = std::round(speed * cos(angle * M_PI / 180));
		if (moveYBelief != 0)
		{
			moveYBelief *= -1; // times -1 because x and y values are flipped in drawing versus normal math
		}

		float moveChance = generateUniformDist(1, 10);
		moveXReal = moveXBelief;
		moveYReal = moveYBelief;

		if (moveChance <= firstChance * 10)
		{
			moveXReal = generateUniformDist(moveXBelief - firstRange, moveXBelief + firstRange);
			moveYReal = generateUniformDist(moveYBelief - firstRange, moveYBelief + firstRange);
		}
		else if (moveChance > firstChance * 10 && moveChance <= secondChance * 10)
		{
			while (moveXReal != moveXBelief - secondRange && moveXReal != moveXBelief + secondRange && moveYReal != moveYBelief - secondRange && moveYReal != moveYBelief + secondRange)
			{
				moveXReal = generateUniformDist(moveXBelief - secondRange, moveXBelief + secondRange);
				moveYReal = generateUniformDist(moveYBelief - secondRange, moveYBelief + secondRange);
			}
		}

		Point nextLocation = Point(position.x + moveXReal, position.y + moveYReal);

		savePrevPosition();
		//front = BoundedVector(nextLocation, position);
		//position.x = nextLocation.x;
		//position.y = nextLocation.y;
		//robotPath.push_back(position);
		notifyObservers();

		moveXMeasured = std::round(generateOdometerVal() * sin(generateCompassVal() * M_PI / 180));
		moveYMeasured = std::round(generateOdometerVal() * cos(generateCompassVal() * M_PI / 180));
		if (moveYMeasured != 0)
		{
			moveYMeasured *= -1;
		}
	}

	void Robot::cleanup()
	{
		robotPathKalman.clear();
		robotPathParticle.clear();
		particles.clear();
	}

} // namespace Model
